using EfCoreIntroduction.CodeFirst.Entities;
using EfCoreIntroduction.WebApp.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContextPool<CodeFirstContext>(options =>
{
    var connString = builder.Configuration.GetConnectionString("DbFirst");

    options.UseNpgsql(connString);
    // Read-only queries are the most executed queries in almost applications.
    // Set the tracking behavior to no tracking to prevent any unoptimized read-only queries.
    // Since update and delete are very sensitive processes, then it recommended
    // to always attach the tracking manually on the said codes.
    options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);

    options.UseSnakeCaseNamingConvention();
});

builder.Services.AddTransient<CompanyCrudService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();
