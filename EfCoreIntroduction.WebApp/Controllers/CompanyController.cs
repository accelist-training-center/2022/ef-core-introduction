﻿using EfCoreIntroduction.WebApp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EfCoreIntroduction.WebApp.Controllers
{
    [Route("api/company")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private readonly CompanyCrudService _companyCrud;

        public CompanyController(CompanyCrudService companyCrudService)
        {
            _companyCrud = companyCrudService;
        }

        [HttpGet]
        public async Task<ActionResult> Get()
        {
            var companies = await _companyCrud.GetAsync();

            return Ok(companies);
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] string companyName)
        {
            await _companyCrud.CreateCompanyAsync(companyName);
            return Ok();
        }

        [HttpPut("{companyId}")]
        public async Task<ActionResult> Update(Guid companyId, [FromBody]string newCompanyName)
        {
            await _companyCrud.UpdateCompanyAsync(companyId, newCompanyName);

            return Ok();
        }

        [HttpDelete("{companyId}")]
        public async Task<ActionResult> Delete(Guid companyId)
        {
            await _companyCrud.DeleteCompanyAsync(companyId);

            return Ok();
        }
    }
}
