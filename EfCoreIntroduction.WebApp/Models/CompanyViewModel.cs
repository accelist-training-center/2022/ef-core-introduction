﻿namespace EfCoreIntroduction.WebApp.Models
{
    /// <summary>
    /// Model class for binding the company data obtained from the database.
    /// </summary>
    public class CompanyViewModel
    {
        public Guid CompanyId { get; set; }

        public string CompanyName { get; set; } = string.Empty;
    }
}
