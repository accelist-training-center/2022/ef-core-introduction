﻿using EfCoreIntroduction.CodeFirst.Entities;
using EfCoreIntroduction.WebApp.Models;
using Microsoft.EntityFrameworkCore;

namespace EfCoreIntroduction.WebApp.Services
{
    /// <summary>
    /// A simple CRUD service class for handling company data.
    /// </summary>
    public class CompanyCrudService
    {
        private readonly CodeFirstContext _db;

        public CompanyCrudService(CodeFirstContext db)
        {
            _db = db;
        }

        /// <summary>
        /// Get list of company data from the database.
        /// </summary>
        /// <returns></returns>
        public async Task<List<CompanyViewModel>> GetAsync()
        {
            var companies = await _db.Companies
                .Select(Q => new CompanyViewModel
                {
                    CompanyId = Q.CompanyId,
                    CompanyName = Q.Name
                })
                .ToListAsync();

            return companies;
        }

        /// <summary>
        /// Create and insert a new company data into database.
        /// </summary>
        /// <param name="companyName"></param>
        /// <returns></returns>
        public async Task CreateCompanyAsync(string companyName)
        {
            _db.Companies.Add(new Company
            {
                CompanyId = Guid.NewGuid(),
                Name = companyName
            });

            await _db.SaveChangesAsync();
        }

        /// <summary>
        /// Update the existing company data.
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="newCompanyName"></param>
        /// <returns></returns>
        public async Task<bool> UpdateCompanyAsync(Guid companyId, string newCompanyName)
        {
            var existingCompany = await _db.Companies
                .FirstOrDefaultAsync(Q => Q.CompanyId == companyId);

            if (existingCompany == null)
            {
                return false;
            }

            existingCompany.Name = newCompanyName;

            _db.Companies.Update(existingCompany);

            await _db.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// Delete an existing company data.<para></para>
        /// By default in EF Core, the deletion process requires us to query first the data that will be deleted.<para></para>
        /// If you are going to do a bulk delete, consider using this: https://entityframework-extensions.net/bulk-delete.
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteCompanyAsync(Guid companyId)
        {
            var existingCompany = await _db.Companies
                .FirstOrDefaultAsync(Q => Q.CompanyId == companyId);

            if (existingCompany == null)
            {
                return false;
            }

            _db.Companies.Remove(existingCompany);

            await _db.SaveChangesAsync();

            return true;
        }
    }
}
