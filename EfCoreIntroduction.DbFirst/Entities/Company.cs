﻿using System;
using System.Collections.Generic;

namespace EfCoreIntroduction.DbFirst.Entities
{
    public partial class Company
    {
        public Company()
        {
            Employees = new HashSet<Employee>();
        }

        public Guid CompanyId { get; set; }
        public string Name { get; set; } = null!;
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; } = null!;

        public virtual ICollection<Employee> Employees { get; set; }
    }
}
