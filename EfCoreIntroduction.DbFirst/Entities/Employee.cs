﻿using System;
using System.Collections.Generic;

namespace EfCoreIntroduction.DbFirst.Entities
{
    public partial class Employee
    {
        public Guid EmployeeId { get; set; }
        public Guid? CompanyId { get; set; }
        public string FullName { get; set; } = null!;
        public string Birthday { get; set; } = null!;
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; } = null!;

        public virtual Company? Company { get; set; }
    }
}
