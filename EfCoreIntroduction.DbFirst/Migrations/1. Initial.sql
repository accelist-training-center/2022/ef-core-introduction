﻿CREATE DATABASE ef_core_tutorial;

CREATE TABLE company
(
	company_id UUID
		CONSTRAINT company_pkey PRIMARY KEY,
	
	"name" TEXT NOT NULL,
	
	created_at TIMESTAMPTZ NOT NULL
		DEFAULT CURRENT_TIMESTAMP,
	
	created_by TEXT NOT NULL
		DEFAULT 'SYSTEM'
);

CREATE TABLE employee
(
	employee_id UUID
		CONSTRAINT employee_pkey PRIMARY KEY,
	
	company_id UUID
		CONSTRAINT employee__company_fkey REFERENCES company,
	
	full_name TEXT NOT NULL,
	
	-- Stored in YYYYMMDD format.
	birthday TEXT NOT NULL,
	
	created_at TIMESTAMPTZ NOT NULL
		DEFAULT CURRENT_TIMESTAMP,
	
	created_by TEXT NOT NULL
		DEFAULT 'SYSTEM'
);