﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfCoreIntroduction.CodeFirst.Entities
{
    public class CodeFirstContext : DbContext
    {
        public CodeFirstContext(DbContextOptions<CodeFirstContext> dbContextOptions) : base(dbContextOptions)
        {
        }

        public DbSet<Company> Companies => Set<Company>();
        public DbSet<Employee> Employees => Set<Employee>();

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Configure our database structure using Fluent API.
            modelBuilder.Entity<Company>(entity =>
            {
                // Set the table name.
                entity.ToTable("company");

                // Set the PRIMARY KEY constraint name.
                entity.HasKey(c => c.CompanyId)
                    .HasName("company_pkey");

                // Set the DEFAULT constraint value using comptabile SQL syntax.
                entity.Property(c => c.CreatedAt)
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                // Set the DEFAULT constraint value.
                entity.Property(c => c.CreatedBy)
                    .HasDefaultValue("SYSTEM");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.ToTable("employee");

                entity.HasKey(e => e.EmployeeId)
                    .HasName("employee_pkey");

                // Set the FOREIGN KEY constraint name.
                entity.HasOne<Company>(e => e.Company)
                    .WithMany(c => c.Employees)
                    .HasForeignKey(e => e.CompanyId)
                    .HasConstraintName("employee__company_fkey");

                entity.Property(e => e.CreatedAt)
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.CreatedBy)
                    .HasDefaultValue("SYSTEM");
            });
        }
    }
}
