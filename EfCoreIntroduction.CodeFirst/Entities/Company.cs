﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfCoreIntroduction.CodeFirst.Entities
{
    public class Company
    {
        public Guid CompanyId { get; set; }

        public string Name { get; set; } = null!;

        public DateTime CreatedAt { get; set; }

        public string CreatedBy { get; set; } = null!;

        public List<Employee> Employees { get; set; } = new List<Employee>();
    }
}
