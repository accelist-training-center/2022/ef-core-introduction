﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfCoreIntroduction.CodeFirst.Entities
{
    [Comment("Store the employee data.")]
    public class Employee
    {
        public Guid EmployeeId { get; set; }

        public Guid CompanyId { get; set; }
        public Company Company { get; set; } = null!;

        [Comment("Store the employee full name.")]
        public string FullName { get; set; } = null!;

        public string Birthday { get; set; } = null!;

        public DateTime CreatedAt { get; set; }

        public string CreatedBy { get; set; } = null!;
    }
}
