﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfCoreIntroduction.CodeFirst.Entities
{
    public class CodeFirstContextFactory : IDesignTimeDbContextFactory<CodeFirstContext>
    {
        public CodeFirstContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CodeFirstContext>();
            var connString = "Server=localhost;Port=5432;Database=ef_core_tutorial;User Id=postgres;Password=postgres;";

            optionsBuilder.UseNpgsql(connString)
                .UseSnakeCaseNamingConvention();

            return new CodeFirstContext(optionsBuilder.Options);
        }
    }
}
