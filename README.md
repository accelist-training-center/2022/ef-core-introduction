# EF Core Tutorial
The `EfCoreIntroduction.CodeFirst` project has been configured with local `dotnet-ef` tools installation in the .NET tool manifest section (see `/.config/dotnet-tools.json`). Restore the locally configured tools with command `dotnet tool restore`.

## Add Migration
To add a new migration, run this following command:
```
dotnet dotnet-ef migrations add <NAME>
```
Replace `<NAME>` with the contextual migration name.

To remove the last added new migration, run this following command:
```
dotnet dotnet-ef migrations remove
```

> **NEVER** remove migrations if you have applied the changes into the actual physical database.

## Apply Migrations
To apply the migrations into the actual physical database, run this following command:
```
dotnet dotnet-ef database update --connection <CONNECTION>
```
Replace `<CONNECTION>` with the connection string to the database.